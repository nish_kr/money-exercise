import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { async, inject, TestBed } from '@angular/core/testing';
import { HttpService } from './http.service';

describe('HttpService', () => {
  let service: HttpService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule(
      {
        imports: [
          HttpClientModule,
          HttpClientTestingModule
        ],
      }).compileComponents();
    service = TestBed.inject(HttpService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', async(inject([HttpTestingController, HttpService],
    (httpClient: HttpTestingController, httpService: HttpService) => {
      expect(service).toBeTruthy();
    })));
});
