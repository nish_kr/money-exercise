import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { ACCOUNT_TYPE, ACCOUNT_TYPE_ARRAY, CreateNewAccountDto } from './../model-types';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  budgetId: string;
  createAccountDto: CreateNewAccountDto;
  accountType = ACCOUNT_TYPE_ARRAY;
  type = ACCOUNT_TYPE;

  @ViewChild('form') public ngForm: NgForm;

  constructor(
    private http: HttpService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.budgetId = localStorage.getItem('budgetId');
    this.createAccountDto = new CreateNewAccountDto();
  }

  createAccount() {
    // this.createAccountDto.type = ACCOUNT_TYPE[this.createAccountDto.type];
    console.log(this.createAccountDto);

    this.http.createNewAccount(this.budgetId, { account: this.createAccountDto }).subscribe(
      (data: any) => {
        console.log(data);
        this.router.navigateByUrl('view-account');
      },
      error => {
        console.log(error);
      }
    )
  }
}
