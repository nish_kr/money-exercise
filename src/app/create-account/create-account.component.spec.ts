import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpService } from '../http.service';
import { CreateAccountComponent } from './create-account.component';


describe('CreateAccountComponent', () => {
  let component: CreateAccountComponent;
  let fixture: ComponentFixture<CreateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, FormsModule],
      providers: [HttpService],
      declarations: [CreateAccountComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not valid', () => {
    // console.log(component.ngForm.form.controls)
    // expect(component.ngForm.form.valid).not.toBeTruthy();
  });

  it('should valid', () => {
    const inputElement = document.getElementById("name");
    console.log(component.ngForm.form.value, inputElement);
    // inputElement.value = "asdf";
    inputElement.dispatchEvent(new Event('input'));
    console.log(component.ngForm.form.value);
    expect(component.ngForm.form.valid).toBeTruthy();
    // inputElement.value = "";
    inputElement.dispatchEvent(new Event('input'));
  });
});
