import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { BudgetAccount } from '../model-types';

@Component({
  selector: 'app-budget-accounts',
  templateUrl: './budget-accounts.component.html',
  styleUrls: ['./budget-accounts.component.scss']
})
export class BudgetAccountsComponent implements OnInit {

  budgetId: string;
  currency: string;
  budgetAccount: BudgetAccount;

  constructor(
    private http: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.budgetId = localStorage.getItem('budgetId');
    this.currency = localStorage.getItem('currency_symbol');
    this.viewAccounts();
  }

  viewAccounts() {
    this.http.showBudgetAccounts(this.budgetId).subscribe(
      (data: any) => {
        this.budgetAccount = data.data as BudgetAccount;
        this.budgetAccount.accounts.sort((a, b) => b.balance - a.balance);
        console.log(this.budgetAccount);
      },
      error => {
        console.log(error);
      }
    )
  }

  createAccount() {
    this.router.navigateByUrl('create-account');
  }
}
