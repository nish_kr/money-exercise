import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { ListOPayees } from './../model-types';

@Component({
  selector: 'app-list-payees',
  templateUrl: './list-payees.component.html',
  styleUrls: ['./list-payees.component.scss']
})
export class ListPayeesComponent implements OnInit {

  budgetId: string;
  listOPayees: ListOPayees;

  constructor(
    private http: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.budgetId = localStorage.getItem('budgetId');
    this.showListOfPayees();
  }

  showListOfPayees() {
    this.http.showListOfPayees(this.budgetId).subscribe(
      (data: any) => {
        console.log(data);
        this.listOPayees = data.data as ListOPayees;
      },
      error => {
        console.log(error);
      }
    )
  }
}
