import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private readonly BASE_URL_BUDGET = 'https://api.youneedabudget.com/v1/budgets/';
  private readonly BASE_URLBugetAccounts = 'https://api.youneedabudget.com/v1/budgets/9f699512-f79a-4954-897f-0e5fdc4502eb/accounts';
  private readonly BASE_URLCreatePostAccount = 'https://api.youneedabudget.com/v1/budgets/9f699512-f79a-4954-897f-0e5fdc4502eb/accounts';
  private readonly token = 'db05cd29a97f7ce8908a02e25abcee56d56d90327d9fddda86b257ee68ee3e1c';

  constructor(
    private httpClient: HttpClient,
  ) { }

  viewAllBudget() {
    return this.httpClient.get(this.BASE_URL_BUDGET, {
      headers: new HttpHeaders().append(
        'Authorization',
        'Bearer ' + this.token
      ),
    });
  }

  showBudgetAccounts(budgetId: string) {
    return this.httpClient.get(this.BASE_URL_BUDGET + budgetId + '/accounts', {
      headers: new HttpHeaders().append(
        'Authorization',
        'Bearer ' + this.token
      ),
    });
  }

  createNewAccount(budgetId: string, accountData) {
    return this.httpClient.post(this.BASE_URL_BUDGET + budgetId + '/accounts', accountData, {
      headers: new HttpHeaders().append(
        'Authorization',
        'Bearer ' + this.token
      ),
    });
  }

  showListOfPayees(budgetId: string) {
    return this.httpClient.get(this.BASE_URL_BUDGET + budgetId + '/payees', {
      headers: new HttpHeaders().append(
        'Authorization',
        'Bearer ' + this.token
      ),
    });
  }
}
