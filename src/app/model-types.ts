export enum ACCOUNT_TYPE {
    checking,
    savings,
    cash,
    creditCard,
    lineOfCredit,
    otherAsset,
    otherLiability
}

export const ACCOUNT_TYPE_ARRAY = [
    'Checking',
    'Savings',
    'Cash',
    'Credit Card',
    'Line Of Credit',
    'Other Asset',
    'Other Liability'
]

export class CreateNewAccountDto {
    name: string;
    type: string;
    balance: number;
}

export class DateFormat {
    format: string;
}

export class CurrencyFormat {
    iso_code: string;
    example_format: string;
    decimal_digits: number;
    decimal_separator: string;
    symbol_first: boolean;
    group_separator: string;
    currency_symbol: string;
    display_symbol: boolean;
}

export class Budgets {
    id: string;
    name: string;
    last_modified_on: string;
    first_month: string;
    last_month: string;
    date_format: DateFormat;
    currency_format: CurrencyFormat;
}

export class ListBudgets {
    budgets: Budgets[];
    default_budget: string;
}

export class Account {
    id: string;
    name: string;
    type: ACCOUNT_TYPE;
    on_budget: boolean;
    closed: boolean;
    note: string;
    balance: number;
    cleared_balance: number;
    uncleared_balance: number;
    transfer_payee_id: string;
    direct_import_linked: boolean;
    direct_import_in_error: boolean;
    deleted: boolean;
}

export class BudgetAccount {
    accounts: Account[];
    server_knowledge: number;
}

export class Payee {
    id: string;
    name: string;
    transfer_account_id: string;
    deleted: boolean;
}

export class ListOPayees {
    payees: Payee[];
    server_knowledge: number
}