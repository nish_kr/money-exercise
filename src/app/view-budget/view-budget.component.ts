import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { Budgets, ListBudgets } from '../model-types';

@Component({
  selector: 'app-view-budget',
  templateUrl: './view-budget.component.html',
  styleUrls: ['./view-budget.component.scss']
})
export class ViewBudgetComponent implements OnInit {

  listBudgets: ListBudgets;

  constructor(
    private http: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getViewBudgetData();
  }

  getViewBudgetData() {
    this.http.viewAllBudget().subscribe(
      (data: any) => {
        console.log(data);
        this.listBudgets = data.data as ListBudgets;
      },
      error => {
        console.log(error);
      }
    )
  }

  viewBudgetAccounts(budgetId: string, currency: string) {
    localStorage.setItem('budgetId', budgetId);
    localStorage.setItem('currency_symbol', currency);
    this.router.navigateByUrl('view-account');
  }

  listPayees(budgetId: string) {
    localStorage.setItem('budgetId', budgetId);
    this.router.navigateByUrl('list-payees');
  }
}
