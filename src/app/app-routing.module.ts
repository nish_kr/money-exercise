import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BudgetAccountsComponent } from './budget-accounts/budget-accounts.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { ListPayeesComponent } from './list-payees/list-payees.component';
import { ViewBudgetComponent } from './view-budget/view-budget.component';

const routes: Routes = [
  {
    path: '',
    component: ViewBudgetComponent
  },
  {
    path: 'view-budget',
    component: ViewBudgetComponent
  },
  {
    path: 'view-account',
    component: BudgetAccountsComponent
  },
  {
    path: 'create-account',
    component: CreateAccountComponent
  },
  {
    path: 'list-payees',
    component: ListPayeesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
